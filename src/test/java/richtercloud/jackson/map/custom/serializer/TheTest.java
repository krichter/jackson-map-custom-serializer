package richtercloud.jackson.map.custom.serializer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author richter
 */
public class TheTest {

    /**
     * Test of setId method, of class Entity2.
     */
    @Test
    public void testSerialization() throws JsonProcessingException, IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Entity1 entity1 = new Entity1(1l);
        Entity2 entity2 = new Entity2(2l);
        entity1.getValueMap().put(entity2, 10);
        String serialized = objectMapper.writeValueAsString(entity1);
        Entity1 deserialized = objectMapper.readValue(serialized, Entity1.class);
        assertEquals(entity1,
                deserialized);
    }
}
